//
//  OnLocationManagerDelegate.swift
//  LocationLibrary
//
//  Created by Israel Flores on 30/11/20.
//

import Foundation
import CoreLocation

public protocol OnLocationManagerDelegate {
    func onSuccess(location: CLLocationCoordinate2D)
    func onError(msg: String)
    func onNotAuthorized()
}
