//
//  LocationUserApi.swift
//  LocationLibrary
//
//  Created by Israel Flores on 01/12/20.
//

import Foundation

open class LocationUserApi {
    
    var locManager: LocationManager? = nil
    
    public init() {
        locManager = LocationManager.shared
    }

    public func startGetLocation(delegate: OnLocationManagerDelegate?) {
        
        locManager?.delegate = delegate
        locManager?.startGetLocation()
    }
    
    public func startUpdating() {
        locManager?.startUpdating()
    }
    
    public func stopUpdating() {
        locManager?.stopUpdating()
    }
    
    public func destroy() {
        stopUpdating()
        locManager = nil
    }
}
