//
//  Network.swift
//  LocationLibrary
//
//  Created by Israel Flores on 07/12/20.
//

import Foundation

enum Network: String {
    case wifi = "en0"
    case cellular = "pdp_ip0"
    //... case ipv4 = "ipv4"
    //... case ipv6 = "ipv6"
}
