//
//  LocationManager.swift
//  LocationLibrary
//
//  Created by Israel Flores on 30/11/20.
//

import CoreLocation

class LocationManager: NSObject {
    
    public static let shared = LocationManager()
    public var locationManager: CLLocationManager? = nil
    
    private var userLocation: CLLocationCoordinate2D? = nil
    var delegate: OnLocationManagerDelegate? = nil
    
    public override init() {
        super.init()
    }
    
    private func initLocationManager(){
        locationManager = CLLocationManager()
        locationManager!.desiredAccuracy = kCLLocationAccuracyBest
        //locationManager!.allowsBackgroundLocationUpdates = true
        //locationManager!.pausesLocationUpdatesAutomatically = false
    }
    
    func startGetLocation(/*delegate: OnLocationManagerDelegate?*/) {
        //self.delegate = delegate
        
        initLocationManager()
        locationManager?.delegate = self
        // Ask for Authorisation from the User.
        locationManager?.requestAlwaysAuthorization()
        // For use in foreground
        locationManager?.requestWhenInUseAuthorization()
        
        print("startGetLocation")
    }
    
    func startUpdating() {
        locationManager?.startUpdatingLocation()
    }
    
    func stopUpdating() {
        locationManager?.stopUpdatingLocation()
    }
    
    func destroy() {
        stopUpdating()
        locationManager = nil
    }
}

//MARK: - Location Manager Methods

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        self.userLocation = locValue
        self.delegate?.onSuccess(location: locValue)
        //LocationUtils.shared.stopUpdating()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            // Location updates are not authorized.
            stopUpdating()
            
            print("Error locationManager")
            self.delegate?.onNotAuthorized()
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse || status == .authorizedAlways else {
            print("Error Authorization location")
            self.delegate?.onNotAuthorized()
            return
        }
        if CLLocationManager.locationServicesEnabled() {
            startUpdating()
        } else {
            print("Error location location")
            self.delegate?.onError(msg: "Error location disabled")
        }
    }
}
