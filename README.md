# LocationLibrary iOS

Librería para obtener la ubicación del usuario, importando la librería estatica al proyecto

Paso 1. Crear nuevo projecto en Xcode (Si ya existe, omitir este paso).

Paso 2. Crear un nuevo folder en la raiz del proyecto llamada "lib".

Paso 3. Descargar y compilar la librería estática con Xcode. Esto generará un archivo llamado "libLocationLibrary.a" detro de la carpeta "Products". Dar clic derecho -> Show in Finder

Paso 4. Copiar y pegar el archivo libLocationLibrary.a y el folder LocationLibrary.swiftmodule dentro del folder "lib" creado en el Paso 2.

Paso 5. Regresa a Xcode y dar click-derecho en el nombre del proyecto nuevamente, y seleccionar la opción "Add Files to 'ProjectName'". 

Paso 6. En la ventana que aparece, seleccionar el folder "lib" y asegurarse que el radio boton "Create groups" está seleccionando.

Paso 7. Verificar que la librería se haya agregado correctamente de la siguiente manera: Ir al navegador de proyecto, seleccionar "General" y el target de la aplicación. Ir a la sección de "Linked Frameworks and Libraries" y asegurarse que contenga la línea "libLocationLibrary.a". Si no aparece, presiona el botón "+" y seleccionala manualmente.

Paso 8. Del mismo modo, verificar que en la pestaña de "Build Phases", "Link Binary with Libraries" contenga la línea "libLocationLibrary.a", de lo contrario, añadir manualmente.

Paso 9. Finalmente, en la pestaña "Build Settings",  En el buscador ingrese "Search Paths". Copia el path de "Library Search Path" y pégalo en "Import Paths" es decir, en ambos casos debe tener $(PROJECT_DIR)/lib

# Ejemplo de uso:

Listo. Ahora puedes importar y utilizar la libraría de la siguiente manera:

1. En el archivo Info.plist, agrega los keys necesarios para obtener la ubicación del usuario:


        <key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
        <string>.....</string>
        <key>NSLocationAlwaysUsageDescription</key>
        <string>.....</string>
        <key>NSLocationWhenInUseUsageDescription</key>
        <string>.....</string>

2. Importa tanto CoreLocation como LocationLibrary a tu clase:
 
        import CoreLocation
        import LocationLibrary

3. Para comenzar la obtención de la ubicación, utiliza la clase "LocationUserApi" y los métodos "startGetLocation(...)" y para detener utiliza "stopUpdating()":

        override func viewDidLoad() {
            super.viewDidLoad()
        
            let locManager = LocationUserApi()
            locManager.startGetLocation(delegate: self)
        }

4. En el ViewController donde requieres obtener la ubicación del usuario, utiliza la siguiente extensión:   
        
        //MARK: - OnLocationManagerDelegate Methods

        extension HomeVC: OnLocationManagerDelegate {

            func onSuccess(location: CLLocationCoordinate2D) {
                print("onSuccess")
            }

            func onError(msg: String) {
                print("onError: \(msg)")
            }
        
            func onNotAuthorized() {
                print("notAuthorized")
            }
        }

# Obtener Dirección IP de RED y VPN

        lblIpAddress.text = NetworkHelper.getIpAddress()
        lblVpn.text = NetworkHelper.isVpnNetwork() ? "true" : "false"


# Referencias:

- https://medium.com/better-programming/create-swift-5-static-library-f1c7a1be3e45