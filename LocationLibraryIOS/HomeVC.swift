//
//  HomeVC.swift
//  LocationLibrarySample
//
//  Created by Israel Flores on 01/12/20.
//

import UIKit
import CoreLocation
import LocationLibrary

class HomeVC: UIViewController {
    
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblIpAddress: UILabel!
    @IBOutlet weak var lblVpn: UILabel!
    
    private var textLoc = ""
    private var isFinish = false
    private let locManager = LocationUserApi()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.barTintColor = UIColor.purple
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)
        ]
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        self.title = "Location Library Sample"
        
        lblLocation.text = textLoc
        lblIpAddress.text = NetworkHelper.getIpAddress()
        lblVpn.text = NetworkHelper.isVpnNetwork() ? "true" : "false"
        
        showMenu()
    }
    
    @IBAction func onBtnLocation(_ sender: Any) {
        if isFinish {
            isFinish = false
            locManager.stopUpdating()
            lblLocation.text = ""
            textLoc = ""
            btnLocation.setTitle("Obtener Ubicación", for: .normal)
        } else {
            locManager.startGetLocation(delegate: self)
        }
    }
    
    func showMenu() {
        let itemRight = UIBarButtonItem(image: UIImage(named: "ic_action_refresh"), style: .plain, target: self, action: #selector(onBtnRefresh))
        //itemRight.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = itemRight
        
        self.navigationItem.rightBarButtonItem = itemRight
        self.navigationItem.setRightBarButton(itemRight, animated: true)
    }
    
    @objc func onBtnRefresh() {
        //Here update data
        lblIpAddress.text = NetworkHelper.getIpAddress()
        lblVpn.text = NetworkHelper.isVpnNetwork() ? "true" : "false"
        self.view.makeToast("Datos actualizados")
    }
    
}


//MARK: - OnLocationManagerDelegate Methods

extension HomeVC: OnLocationManagerDelegate {
    func onSuccess(location: CLLocationCoordinate2D) {
        print("onSuccess")
        
        textLoc += "\nlatitud: \(location.latitude), longitud: \(location.longitude)"
        lblLocation.text = textLoc
        
        //detener
        btnLocation.setTitle("Detener", for: .normal)
        isFinish = true
    }
    
    func onError(msg: String) {
        print("onError: \(msg)")
        lblLocation.text = "Falló la obtención de ubicación.intente de nuevo por favor"
    }
    
    func onNotAuthorized() {
        print("notAuthorized")
        lblLocation.text = "Permiso denegado, activar manualmente"
    }
}

